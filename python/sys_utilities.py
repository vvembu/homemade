#!/usr/bin/python

import pyperclip, re
from pynput.keyboard import Key,Controller
from platform import system

def getMatch(patterns, text):

    result={i : [] for i in list(patterns.keys())}

    for pattern in patterns.keys():

        result[pattern].extend(re.compile(patterns[pattern]).findall(text))

    return result

def getChoice():

    pass

def displayNotification():

    pass

if __name__ == '__main__':

    #controller object
    keyboard = Controller()

    #delay
    keyboard_dealy=0.5

    ''' patterns '''

    patterns={

        'disk_serial' : r'(\s?[\'\"]?[A-Z\d]+[-]?[_]?[A-Z\d]{6,50}[,.]?[\'\"]?)',
        'ipv4' : r'(\d{1,3}\.){3}\d{1,3}',
        'ticket' : r'[A-Z]{1}-[A-Z]{1}-[A-Z0-9]+'
    }

    ''' Press and realease  keys '''

    if system() != "Darwin":

        #simulate ctrl +c
        keyboard.press(Key.ctrl)
        keyboard.press('c')

        #sleep
        sleep(keyboard_dealy)

        #release keys
        keyboard.release('c')
        keyboard.release(Key.ctrl)

    else:

        #simulate cmd +c
        keyboard.press(Key.cmd)
        keyboard.press('c')

        #sleep
        sleep(keyboard_dealy)

        #release keys
        keyboard.release('c')
        keyboard.release(Key.cmd)

    ''' get all matched elements ''

    matched=getMatch(patterns,pyperclip.copy()
    )
