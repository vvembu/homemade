#!/usr/bin/python3

import sys
import pyautogui as pt
from time import sleep

COORDINATES={
    'menu':
    {
        'x' : 16,
        'y': 1060
    }
}

def openWhiskerMenu():

    #click on whisker menu
    pt.click(COORDINATES['menu']['x'], COORDINATES['menu']['y'],
    button='left')

    #sleep 1 sec
    sleep(1)

def openAtom():

    #open Menu

    openWhiskerMenu()

    #type rys
    pt.typewrite('atom', interval=0.25)

    #press enter
    pt.hotkey('enter')

    #sleep 3 sec
    sleep(3)

def openSpotify():

    #open Menu

    openWhiskerMenu()

    #type rys
    pt.typewrite('spotify', interval=0.25)

    #press enter
    pt.hotkey('enter')

    #sleep 1 sec
    sleep(1)

def openTerminal():

    #open Menu

    openWhiskerMenu()

    #type rys
    pt.typewrite('terminal', interval=0.25)

    #press enter
    pt.hotkey('enter')


def openChromium():

    #open Menu

    openWhiskerMenu()

    #type rys
    pt.typewrite('chromium', interval=0.25)

    #press enter
    pt.hotkey('enter')

    #sleep 1 sec
    sleep(1)

def openThunderbird():

    #open Menu

    openWhiskerMenu()

    #type rys
    pt.typewrite('thunderbird', interval=0.25)

    #press enter
    pt.hotkey('enter')

    #sleep 1 sec
    sleep(0.3)


if __name__ == '__main__':

    ''' open Atom '''

    openAtom()

    ''' Open spotify '''

    openSpotify()

    ''' Open Thunder bird '''

    openThunderbird()

    ''' Open Chromium '''

    openChromium()

    ''' Open Terminal '''

    openTerminal()
