#!/usr/bin/python3
import random, sys, os, math
import pyperclip
from time import time

def pp(dd,lw,rw):

    print("Na piknik".center(lw+rw,'-'))

    for k,v in dd.items():
        print(k.ljust(lw,'.')+str(v).rjust(rw))



if __name__ == '__main__':

    print("*"*10)

    ''' coutning down '''
    print("counting down from 10 to 0")
    for i in range(10,-1,-1):

        print("count: ",i," random int ",random.randint(i,100))

    ''' ljust, rjust, center '''
    txt='''
    ljust
    rjust
    center'''
    print(txt)
    print(" center ".center(len("center")+10,'*'))
    print(" ljust ".ljust(len("ljust")+10,'*'))
    print(" rjust ".rjust(len("rjust")+10,'*'))

    ''' more pretty output '''
    dd={
        'kanapki': 4,
        'jablka' : 12,
        'kubki' : 4,
        'ciastka' : 8000
    }

    pp(dd,12,5)
    pp(dd,20,6)

    orig_str="Original: < I want to delete aaaaa>aaaaa"
    mod_str=orig_str.strip("a")
    print(orig_str,
    "\nAfter mod:\n",
    mod_str)

    ''' clipboard '''

    '''
        NAME             FSTYPE      LABEL  UUID                                   FSAVAIL FSUSE% MOUNTPOINT
    crypto           LVM2_member        qIZbvo-CB77-yON0-2NZ4-fHDf-bpRS-ON2Msu
    crypto--lvm-boot xfs                7e983b3f-3302-497b-976c-775354b27acc    891,2M    12% /boot
    crypto--lvm-root xfs                d915ecb3-6652-4851-b678-3d4238cff2d7     13,2G    56% /
    crypto--lvm-home xfs                880c36bd-4dd6-44f9-b1bc-6006c3d910c1    158,8G    23% /home
    nvme0n1
    nvme0n1p1        vfat        SYSTEM FA84-86F2                               218,4M    15% /boot/efi
    nvme0n1p2        crypto_LUKS        c32f0831-684a-4828-a548-ea24940682d1
    '''
    st=time()
    out_dd=[i.split(' ')[0]  for i in pyperclip.paste().split('\n') \
    if i.split(' ')[0].lower() != 'name']
    print("processing wiht copying: ",time()-st)

    st=time()
    pyperclip.copy(' '.join(out_dd))
    print("pasting: ",time()-st)
