#!/usr/bin/python3

from time import time
from string import Template
import sys, os, argparse
import xml.etree.ElementTree as ET

''' global varables neccessary functions '''

def isPath(path):

    if os.path.exists(path):

        return path
    else:

        message=format(
        "\nPath: "+path
        +" doesn't exist !!!!"
        )

        raise argparse.ArgumentTypeError(message)

''' argparse '''

parser = argparse.ArgumentParser()
parser.add_argument('--template', nargs='?', help='template (string)')
parser.add_argument('--template-dir', dest='template_dir',
    help='Full path to dir with templates', type=isPath,
    nargs='?')
ARGS=parser.parse_args()

''' Global options '''

OPTIONS={

    'getTemplates':
    {
        'file_extension' : '.xml',
        'template_dir'   : '/tmp/templates' if not \
        ARGS.template_dir else ARGS.template_dir,
        'template-name' : 'name',
        'template-number' : 'number',
        'template-text' : 'text',
        'template-variables' : 'variables',
        'dict-key' : 'name'
    }
}

def getTemplates(options):

    #result
    result={}

    if not options['template_dir'].endswith('/'):

        options['template_dir']+='/'

    try:

        filenames=[options['template_dir']+file \
        for file in os.listdir(options['template_dir']) \
        if file.endswith(options['file_extension'])
        ]

    except:

        pass

        print("Could not list files in dir: ",
        options['template_dir'],
        "\n",sys.exc_info()
        )

    ''' load templates from file '''

    for filename in filenames:

        try:

            #load xml file
            root=ET.parse(filename).getroot()

            #get template as options['dict-key']
            template_key_name=root.find(options['dict-key'])

            if hasattr(template_key_name, 'text'):

                ''' #creating new dict with name as key taken,
                 from option dictionary and trying to convert to float
                 if number
                 '''

                if options['dict-key'] == options['template-number']:

                    try:

                        #template key
                        template_key=float(template_key_name.text)

                    except:

                        pass

                        #template key
                        template_key=template_key_name.text

                else:

                    #template key
                    template_key=template_key_name.text

                result[template_key]={
                    options['dict-key'] : template_key
                }

                ''' get element  options['template-name'] '''

                template_name=root.find(options['template-name'])

                if hasattr(template_name, 'text'):

                    result[template_key][options['template-name']]=\
                    template_name.text

                ''' get element  options['template-number']  '''

                template_number=root.find(options['template-number'])

                if hasattr(template_number, 'text'):

                    try:

                        result[template_key][options['template-number']]=\
                        float(template_number.text)

                    except:

                        pass
                        result[template_key][options['template-number']]=\
                        template_number.text

                ''' get element options['template_text'] '''

                template_text=root.find(options['template-text'])

                if hasattr(template_text, 'text'):

                    result[template_key][options['template-text']]=\
                    template_text.text

                ''' if variables available in template '''

                template_variables=root.find(options['template-variables'])

                #if any element of options['template_variables']
                if template_variables:

                    #create empty dict with variables
                    result[template_key][options['template-variables']]={}

                    ''' assing values to dict with variables '''

                    for variable in template_variables:

                        if (variable.attrib and variable.text
                        and hasattr(variable, 'attrib')
                        and (variable, 'text')):

                            result[template_key]\
                            [options['template-variables']]\
                            [variable.attrib['name']]=variable.text
            else:

                print("Could not assign the values.",
                "\nPlease check the xml file: \n",filename)
        except:

            pass

            print("Could load template from file: ",
            filename,"\n",sys.exc_info()
            )

    return result

if __name__ == '__main__':

    #get templates
    TEMPLATES=getTemplates(OPTIONS['getTemplates'])

    ''' template string part '''

    print("Before Replace: ",
    TEMPLATES['CosMojego2']['text'])

    ''' changes mapping variable name to variable value '''

    TEMPLATES['CosMojego2']['variables']['animal']='Cat'
    TEMPLATES['CosMojego2']['variables']['animal2']='Lynx'

    ''' replace variable name by variable value '''

    TEMPLATES['CosMojego2']['text']=Template(
    TEMPLATES['CosMojego2']['text']).substitute(
    **TEMPLATES['CosMojego2']['variables']
    )

    print("After Replace: ",
    TEMPLATES['CosMojego2']['text'])
