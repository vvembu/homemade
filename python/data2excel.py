#!/usr/bin/python3

import xlsxwriter
import sys
from collections import OrderedDict

def data2Excel(dataset, file, fieldnames):

    ''' creating workbook '''

    wb=xlsxwriter.Workbook(file)

    ''' setting cell format '''

    ''' default format '''

    #setting border
    cell_format=wb.add_format({'border': 2})

    #wrap text in a cell
    cell_format.set_text_wrap()

    #align vertical center
    cell_format.set_align('top')

    ''' header  format '''

    #setting border
    header_format=wb.add_format({'border': 2})

    #align vertical center
    header_format.set_align('top')

    #font size
    header_format.set_font_size(14)

    #bg color
    header_format.set_bg_color('#D3D3D3')

    ''' work sheets '''

    ws = {}

    for data in dataset.values():

        if not data["sheet-name"] in ws.keys():

            ''' creating work sheet if not exist '''

            ws[data['sheet-name']]={
            'worksheet' : wb.add_worksheet(data['sheet-name']),
            'row' : 0,
            }

            ''' adding header '''

            for column, fieldname in enumerate(fieldnames.keys(), 0):

                ''' writing to columns at row 0 '''

                ws[data['sheet-name']]['worksheet'].write(
                ws[data['sheet-name']]['row'],
                column,
                fieldnames[fieldname],
                header_format
                )

            #increase row number()
            ws[data['sheet-name']]['row']+=1

        ''' adding data '''

        for column, fieldname in  enumerate(fieldnames.keys(), 0):

            ''' writing to columns at row N '''

            ws[data['sheet-name']]['worksheet'].write(
            ws[data['sheet-name']]['row'],
            column,
            data[fieldname],
            cell_format
            )

        #increase row number
        ws[data['sheet-name']]['row']+=1

    '''
    autofilter -> turning on

    first row,
    first column,
    last row,
    last column
    '''

    ''' enabling autofilter and setting  width of a column'''

    for i in ws.keys():

        ''' enabling autofilter in worksheet '''

        ws[i]['worksheet'].autofilter(0, 0, ws[i]['row'],
        len(fieldnames.keys())-1
        )

        ''' setting width size of a column
        as size of header column +10
        '''

        for column, fieldname in  enumerate(fieldnames.keys(), 0):

            ws[i]['worksheet'].set_column(
            column, column, len(str(fieldnames[fieldname]))+10
            )

    ''' closing wb file '''

    wb.close()


if __name__ == '__main__':

    filename = '/tmp/data.xlsx'

    fieldnames=OrderedDict({

        'name' : 'Name',
        'surname' : 'Surname',
        'shoe-size' : 'Shoe Size',
        'phone-extension' : 'Phone Extension',
        'phone-number' : 'Phone Number'
    })

    dataset={

        'data1':
        {
            'sheet-name' : 'Secure',
            'name' : 'Adam',
            'surname' : 'SomeSurname2hhbhebehbfhbfhbfhebhebfhebfhbfhebfhebhebhe',
            'shoe-size' : 39,
            'phone-extension' : '+41' ,
            'phone-number' : '143-222-333'
        },

        'data2':
        {
            'sheet-name' : 'Insecure',
            'name' : 'Alan',
            'surname' : 'SomeSurnamenjnjnjnjnjnjnjnjnnjnjjjnjnjnjn',
            'shoe-size' : 38,
            'phone-extension' : '+48' ,
            'phone-number' : '121-222-333'
        },

        'data3':
        {
            'sheet-name' : 'Secure',
            'name' : 'Tom',
            'surname' : 'SomeSurname2',
            'shoe-size' : 40,
            'phone-extension' : '+58' ,
            'phone-number' : '131-222-333'
        },

        'data4':
        {
            'sheet-name' : 'Insecure',
            'name' : 'Cate',
            'surname' : 'SomeSurname3',
            'shoe-size' : 41,
            'phone-extension' : '+68' ,
            'phone-number' : '151-222-333'
        }
    }

    data2Excel(dataset, filename, fieldnames)
