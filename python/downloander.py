#!/usr/bin/python3
import requests
import argparse
import os
import sys
from time import time
import multiprocessing

''' global varables neccessary functions '''

def isPath(path):

    if os.path.exists(path):

        return path
    else:

        message=format(
        "\nPath: "+path
        +" doesn't exist !!!!"
        )

        raise argparse.ArgumentTypeError(message)

''' argparse '''

parser = argparse.ArgumentParser()
parser.add_argument('urls', nargs='*', help='url1 url2 urlN')
parser.add_argument('-file', '--filename', dest='file',
    help='Full path to file with urls, separator is new line/',
    type=isPath)
parser.add_argument('--download-dir',  dest='download_dir',
    help='Full path to download dir',type=isPath)
parser.add_argument('--chunk-size',  dest='chunk_size',
    help='chunk size of downloaded file', type=int, default=32768)
parser.add_argument('--num-proc',  dest='num_proc',
    help='define number of process', type=int, default=2,
    choices=[1,2,3,4,5,6,7,8])
ARGS=parser.parse_args()

''' functions '''

def download_url(url, download_dir, chunk_size=32768):

    ''' result '''

    result={

        'url' : url,
        'filename' : '',
        'response' : ''
    }

    ''' get file name '''

    filename=url.split("/")

    if filename:

        filename=filename[-1]

    else:

        filename=url

    #filename of downloaded file
    result['filename']=filename

    ''' download and save the file '''

    try:

        with requests.get(url, stream=True) as download:


            with open(download_dir+filename,'wb') as file:

                for chunk in download.iter_content(
                chunk_size=ARGS.chunk_size):

                    file.write(chunk)

                file.close()

            result['resposne']=download.status_code

    except KeyboardInterrupt:

        raise

    except:

        pass
        print("Failed to download a file from:\n",url,
        "\n",sys.exc_info())

    return result

if __name__ == '__main__':

    ''' setting starting method to spwan instead of fork '''

    multiprocessing.set_start_method("spawn")

    urls=[]


    ''' if file is provided '''

    if ARGS.file:

        with open(ARGS.file, 'r', encoding='utf_8') as urls:

            for url in urls.read().split('\n'):

                if url.strip():

                    urls.append(url.strip())

    else:

        urls=ARGS.urls

    ''' download dir '''

    if ARGS.download_dir:

        DOWNLOAD_DIR=ARGS.download_dir \
        if ARGS.download_dir.endswith("/") \
        else ARGS.download_dir+"/"

    else:

        DOWNLOAD_DIR=os.getcwd() \
        if os.getcwd().endswith("/") \
        else os.getcwd()+"/"

    '''multiprocessing config '''

    config=[[url, DOWNLOAD_DIR, ARGS.chunk_size] for url in urls]


    ''' starting pool '''
    st=time()

    #choose proc num

    if ARGS.num_proc <= multiprocessing.cpu_count():

        pool=multiprocessing.Pool(ARGS.num_proc)
        print("Will run with: ",ARGS.num_proc," process(es)")

    else:

        pool=multiprocessing.Pool()
        print("Will run with: ",multiprocessing.cpu_count()," process(es)")


    download=pool.starmap_async(download_url, config)
    downloaded=download.get()

    pool.close()
    pool.join()

    for download in downloaded:

        print("URL:\n\t",download['url'],
        '\nPath:\n\t',download['filename'],
        "\nResponse:\n\t",download['response'])

    print("Elapsed(s): ",round(time()-st,2))
