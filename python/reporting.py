#!/usr/bin/python3

def genReport(data):

    result=''

    ''' if data is not a dict '''

    if type(data).__name__ != 'dict':

        return result
    ''' loop over dict '''

    for key, item in data.items():

        ''' if an item in a dict is a dict '''

        if type(item).__name__ == 'dict':

            ''' if dict has key: text '''

            if item.get("text", False):

                result+=format("\n"+item['text'])

            ''' if dict has data '''

            if item.get('data', False):

                ''' if data is stored in the list '''

                if type(item['data']).__name__ == 'list':

                    ''' if count of data should be displayed '''

                    if item.get('cnt', False):

                        result+=format(str(len(item['data']))+'\n')

                    else:

                        result+=format('\n')

                    ''' add data to the result '''

                    result+=format(
                    '\n'
                    +'\n'.join(str(i) for i in item['data'])+'\n'
                    )

                #if data is a dict
                elif type(item['data']).__name__ == 'dict':

                    #add new line for readiness
                    result+=format('\n')

                    ''' loop over data dict '''

                    for _key, _item in item['data'].items():

                        ''' if count of data should be displayed '''

                        if (item.get('cnt', False)
                        and type(_item).__name__ == 'list'):

                            ''' if text should be displayed before the count '''

                            if item.get('cnt_text', False):

                                result+=format(
                                '\n'+key+item['cnt_text']+str(len(_item))
                                +'\n\n'+'\n'.join(str(i) for i in _item)+'\n'
                                )

                            else:

                                result+=format(
                                '\n'+key+str(len(_item))
                                +'\n\n'+'\n'.join(str(i) for i in _item)+'\n'
                                )
                        else:

                            result+=format(
                            '\n'+key+'\n\n'
                            +'\n'.join(str(i) for i in _item)+'\n'
                            )
    return result

if __name__ == '__main__':

    data={
        'bla':
        {
            'text': 'Smieszne cosie ponizej:) Total:',
            'data' : [1,2,3,4,'piec','szesc'],
            'cnt' : True
        },

        'bla2':
        {
            'text': 'Smieszne cosie ponizej i cos inne:)',
            'data' : {
                '_bla3': [1,2,3,4,5],
                '_bla4' : ['piec','osiem',1]
            },
            'cnt' : True,
            'cnt_text': ' Total: '
        }
    }

print(genReport(data))
