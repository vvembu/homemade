#!/usr/bin/python3

import xlsxwriter
import csv
import argparse
import sys
from ast import literal_eval

def data2Excel(dataset, file):

    ''' if dataset is string not a list '''

    if type(dataset).__name__== 'str':

        try:

            with open(dataset, 'r', encoding='utf_8') as csv_file:

                dataset=list(csv.reader(csv_file))
                csv_file.close()

        except:

            pass

            csv_file.close()
            print("Something went wrong while reading: ",path,
            os.exc_info())

            #exit from function if smth wrong
            return ''

    ''' normal part '''

    tmp=""

    ''' creating workbook '''

    wb=xlsxwriter.Workbook(file)

    ''' adding new worksheet '''

    ws=wb.add_worksheet()

    ''' importing data to xlsx '''

    row=0

    ''' loop over whole list '''

    for record in dataset:

        ''' loop over field in a record line '''

        for column, field in enumerate(record, 0):

            ''' converting field to correct data type '''

            try:

                tmp=field
                field=literal_eval(field)

            except:

                pass
                field=tmp

            ws.write(row, column, field)

        #increase row number
        row+=1

    '''
        enabling autofilter

    first row,
    first column,
    last row,
    last column
    '''

    ws.autofilter(0, 0, row, len(dataset[0])-1)

    ''' closing wb file '''

    wb.close()

if __name__ == '__main__':

    ''' argparse '''

    parser = argparse.ArgumentParser()
    parser.add_argument('files', nargs='+',
        help='Full_path1 full_path2 ... full_path_N')

    ARGS=parser.parse_args()

    ''' file extensions '''

    excel_extension='.xlsx'
    csv_extension='.csv'

    ''' looping over files '''

    for file in ARGS.files:

        ''' importing /creating xslx file '''

        data2Excel(file, file.replace(
        csv_extension, excel_extension)
        )
