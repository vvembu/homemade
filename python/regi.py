import re

def getMatch(patterns, txt):

    return [re.compile(i).search(txt) for i in patterns]

def multiRep(patterns, text):

    result=text

    if type(patterns).__name__ == 'dict':

        for key, value in patterns.items():

            result=re.sub(key, value, result)

    return result

patterns=[r"(\d{2}\s\d{3}-\d{2}-\d{2})", r'\d{3}-\d{3}-\d{3}',
r"\+\d\ds\d{3}-\d{3}-\d{3}"]

phoneregex=re.compile(r'\d\d\d-\d\d\d-\d\d\d')

mo=phoneregex.search("777-111-000")
phone_reg=re.compile(r"(\d\d\s\d\d\d-\d\d-\d\d)")
print(mo.group())

txt="""
This is sample text, example phone number: 000-111-222 or 12 111-22-33
or +48 999-111-222 ; that's all :)
"""
print(getMatch(patterns, txt))

''' or '''

print(re.compile(
r'smieszek|pingwin').search(
"pingwin uzywa smieszek")
)

'''optional group '''
print(
re.compile(r'(\+\d{1,4})?\s(\d{1,3}-){2}\d{1,3}').search(
"EXAMPLE number: +48 721-834-431\n"+
"Example two 111-222-333")
)


'''wildcard zero or more '''
print(
re.compile(r'(numer)*').search(
"Moj numer alalal to bla bla bla")
)

''' plus one or more '''
print(
re.compile(r'(numer)+').search(
"Moj numerblalalal to bla bla bla")
)

''' .* '''
txt='''
<Ala ma kota >
<Kot ma rysia>
<begin>
<end>
'''
print(
re.compile(r'<.*>').findall(txt)
)


''' case insesitive '''

print(re.compile(r'Please Run this cmd',re.I).search("PLEASE RUN THIS CMD"))

''' sed like '''

removeMe={
    'ą' : 'a',
    'ć' : 'c',
    'ź' : 'z',
    'ż' : 'z',
    'ł' : 'l'
}

print(multiRep(removeMe,"łącki żłolekź"))

'''comments in regex '''

print(re.compile(r'''
^ #begin of the string
(Smieszek #string
| #or
Pingwin) #another string
''',re.VERBOSE).sub(r'\1----',"Pingwin cos zrobil"))


''' all opt at once in compile '''

print(re.compile(r'Ala\s(\w+)',re.IGNORECASE|re.DOTALL|re.VERBOSE).search(
"ALA kotecek fajny")
)
