#!/usr/bin/python3

import multiprocessing
from time import sleep
from multiprocessing.dummy import Pool as ThreadPool
import random
import string
import time


class cont:

    def __init__(self, len=20, words=100):

        self.words=words
        self.len=len
        self.text=''
        letters=string.ascii_lowercase

        for i in range(words):

            self.text+=' '+''.join(random.choice(letters) for i in range(len))


def searching(text, pattern):

    if pattern in text:

        return True

    else:

        return False


def searching2(text, pattern, key):

    if pattern in text:

        return {key:True}

    else:

        return {key:False}


if __name__ == '__main__':

    multiprocessing.set_start_method("spawn")
    MAX_NO=4

    conts=[cont() for i in range(10)]
    config=[(i.text, "abc") for i in conts ]
    config2=[(config[i][0],config[i][1],i) for i in range(len(config))]

    '''Process part '''

    st=time.time()

    pool=multiprocessing.Pool(MAX_NO)
    proc=pool.starmap_async(searching, config)

    proc.get()
    pool.close()
    pool.join()

    print("Process version took: ",time.time()-st)

    ''' Thread part '''

    st=time.time()

    pool=ThreadPool(MAX_NO)
    treat=pool.starmap_async(searching, config)

    treat.get()
    pool.close()
    pool.join()


    print("Thread version took: ",time.time()-st)


    ''' get res test '''

    print("\n\ntry to fetch results\n\n")

    st=time.time()

    pool=ThreadPool(MAX_NO)
    treat=pool.starmap_async(searching2, config2)

    treat_res=treat.get()
    pool.close()
    pool.join()

    resy={}

    #short
    _=[resy.update(treat_res[i]) for i in range(len(treat_res))]

    '''
    longer
    for item in treat_res:

        resy.update(item)
    '''
    for i in resy:

        print("key: ",i," val: ",resy[i])
