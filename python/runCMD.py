#!/usr/bin/python3

from time import time
from string import Template
import sys, os, argparse
import xml.etree.ElementTree as ET
import subprocess
from getpass import getpass

''' global varables neccessary functions '''

def isPath(path):

    if os.path.exists(path):

        return path
    else:

        message=format(
        "\nPath: "+path
        +" doesn't exist !!!!"
        )

        raise argparse.ArgumentTypeError(message)

''' argparse '''

parser = argparse.ArgumentParser()
parser.add_argument('ips', nargs='*', help='IP1 IP2 .. IPN')
parser.add_argument('--template-dir', dest='template_dir',
    help='Full path to dir with templates', type=isPath, nargs='?')
parser.add_argument('-file', '--filename', dest='file',
    help='Full path to file with IP adresses, separator is new line/',
    type=isPath)

ARGS=parser.parse_args()

''' Global options '''

OPTIONS={

    'getTemplates':
    {
        'file_extension' : '.xml',
        'template_dir'   : '/tmp/templates' if not \
        ARGS.template_dir else ARGS.template_dir,
        'template-name' : 'name',
        'template-number' : 'number',
        'template-text' : 'text',
        'template-variables' : 'variables',
        'dict-key' : 'name',
        'template-inputs' : 'inputs',
        'template-commands' : 'commands'
    }
}

def getTemplates(options):

    #result
    result={}

    if not options['template_dir'].endswith('/'):

        options['template_dir']+='/'

    try:

        filenames=[options['template_dir']+file \
        for file in os.listdir(options['template_dir']) \
        if file.endswith(options['file_extension'])
        ]

    except:

        pass

        print("Could not list files in dir: ",
        options['template_dir'],
        "\n",sys.exc_info()
        )

    ''' load templates from file '''

    for filename in filenames:

        try:

            #load xml file
            root=ET.parse(filename).getroot()

            #get template as options['dict-key']
            template_key_name=root.find(options['dict-key'])

            if hasattr(template_key_name, 'text'):

                ''' #creating new dict with name as key taken,
                 from option dictionary and trying to convert to float
                 if number
                 '''

                if options['dict-key'] == options['template-number']:

                    try:

                        #template key
                        template_key=float(template_key_name.text)

                    except:

                        pass

                        #template key
                        template_key=template_key_name.text

                else:

                    #template key
                    template_key=template_key_name.text

                result[template_key]={
                    options['dict-key'] : template_key
                }

                ''' get element  options['template-name'] '''

                template_name=root.find(options['template-name'])

                if hasattr(template_name, 'text'):

                    result[template_key][options['template-name']]=\
                    template_name.text

                ''' get element  options['template-number']  '''

                template_number=root.find(options['template-number'])

                if hasattr(template_number, 'text'):

                    try:

                        result[template_key][options['template-number']]=\
                        float(template_number.text)

                    except:

                        pass
                        result[template_key][options['template-number']]=\
                        template_number.text

                ''' get element options['template_text'] '''

                template_text=root.find(options['template-text'])

                if hasattr(template_text, 'text'):

                    result[template_key][options['template-text']]=\
                    template_text.text

                ''' if variables are available in template '''

                template_variables=root.find(options['template-variables'])

                #if any element of options['template_variables']
                if template_variables:

                    #create empty dict with variables
                    result[template_key][options['template-variables']]={}

                    ''' assing values to dict with variables '''

                    for variable in template_variables:

                        if (variable.attrib and variable.text
                        and hasattr(variable, 'attrib')
                        and (variable, 'text')):

                            result[template_key]\
                            [options['template-variables']]\
                            [variable.attrib['name']]=variable.text

                ''' if inputs are in template '''

                template_inputs=root.find(options['template-inputs'])

                #if any element of options['template_inputs']
                if template_inputs:

                    #create empty dict with inputs
                    result[template_key][options['template-inputs']]={}

                    ''' assing values to dict with inputs '''

                    for input in template_inputs:

                        if (input.attrib and input.text
                        and hasattr(input, 'attrib')
                        and (input, 'text')):

                            result[template_key]\
                            [options['template-inputs']]\
                            [input.attrib['name']]=input.text

                ''' if commands are in template '''

                template_commands=root.find(options['template-commands'])

                #if any element of options['template_commands']
                if template_commands:

                    #create empty dict with commands
                    result[template_key][options['template-commands']]={}

                    ''' assing values to dict with commands '''

                    for command in template_commands:

                        if (command.attrib and command.text
                        and hasattr(command, 'attrib')
                        and (command, 'text')):

                            result[template_key]\
                            [options['template-commands']]\
                            [command.attrib['name']]=command.text
            else:

                print("Could not assign the values.",
                "\nPlease check the xml file: \n",filename)
        except:

            pass

            print("Could load template from file: ",
            filename,"\n",sys.exc_info()
            )

    return result

def getInput(message="", **kwargs):

    answer=""

    ''' checking if ask for input is in loop '''

    if kwargs.get("loop", False):

        ''' set defualt message if empty '''

        if not message:

            message=format(
            "Please enter the input (terminate input by typing EOF "
            +"and pressing enter)"
            )

        print(message)

        while "EOF" not in answer:

            if kwargs.get("secure", False):

                answer=getpass(prompt='')

            else:

                answer=input()

        #remove EOF
        answer=answer.replace("EOF","")

    else:

        ''' set defualt message if empty '''

        if not message:

            message=format(
            "Please enter the input (terminate input by pressing enter"
            )

        ''' check if input should be secure '''

        if kwargs.get("secure", False):

            answer=getpass(prompt=message)

        else:

            answer=input(message)

        ''' while input is empty '''

        while not answer:

            ''' check if input should be secure '''

            if kwargs.get("secure", False):

                answer=getpass(prompt=message)

            else:

                answer=input(message)

    ''' check if answer should be converted '''

    if kwargs.get("convert", False):

        if kwargs['convert'].lower() == "int":

            try:

                tmp=answer
                answer=int(answer)

            except:

                pass
                answer=tmp

        elif kwargs['convert'].lower() == "float":

            try:

                tmp=answer
                answer=float(answer)

            except:

                pass
                answer=tmp

    return answer

def menu(templates):

    data_type=set([type(i).__name__ for i in templates.keys()])

    if 'str' in data_type:

        navigation={ key: value[0] for key, value in \
        enumerate(
        sorted(
        templates.items(), key=\
        lambda x: x[1][OPTIONS['getTemplates']['template-number']]),1)
        }

    else:

        navigation={ key: value for key, value in \
        enumerate(sorted(templates.keys()),1)}

    answer=""

    print("Menu: \n")

    for i in sorted(navigation.keys()):

        print(i,".",templates[navigation[i]]['name'])

    #get input
    message=format(
    "\n\nPlease choose your option: (End your input by pressing enter)\n"
    )

    try:

        answer=int(getInput(message))

        while not answer in navigation.keys():

            answer=getInput(message, convert="int")
    except:

        pass

    return navigation[answer]

def runCMD(ip, cmd, **kwargs):

    result={
        'ip' : ip,
        'cmd': cmd,
        'output' : '',
        'status' : ''
    }

    #command
    command=""

    ''' setting command base on kwargs '''

    if kwargs.get("local", False):

            command=format(cmd)
    else:

        if kwargs.get("command_switch", False):

            cmd=format(
            kwargs['command_switch']+" "+cmd
            )

        if kwargs.get("tool", False):

            command=format(
            kwargs['tool']+" "+ip +" "+cmd+" "
            )

            if kwargs.get("reverse", False):

                command=format(
                kwargs['tool']+" "+cmd+" "+ip
                )
        else:

            command=format(
            cmd+" "+ip
            )
    try:

        ''' running cmd '''

        result['output']=subprocess.check_output(command,
        universal_newlines=True,
        shell=True,
        stderr=subprocess.STDOUT
        )

        #setting status to fine
        result['status']='ok'

    except subprocess.CalledProcessError as exc:

        pass

        print("Running cmd: ",exc.cmd,
        "has finished with exit status: ",exc.returncode,
        "\nError: ",exc.output)

        #setting status to failed
        result['status']='failed'

        #clearing result
        result['output']=''

    return result

if __name__ == '__main__':

    ''' if template dir not provided or missing then exit '''

    if (not ARGS.template_dir
    and not os.path.exists(OPTIONS['getTemplates']['template_dir'])):

        print("Missing template directory or not provided",
        "\nDefault (expected) template dir : /tmp/templates")

        sys.exit()

    #loading templates to dictionary
    CMD=getTemplates(OPTIONS['getTemplates'])

    #get key of dict
    key=menu(CMD)

    if CMD[key]['number'] == 1.0 or CMD[key]['number'] == 2.0:

        result=runCMD("someval", CMD[key]['text'], local=True)

    print("Cmd: ", result['cmd'],
    "\nStatus: ", result['status'],
    "\nResult:\n", result['output'])

    print(CMD)
