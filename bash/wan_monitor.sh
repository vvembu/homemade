#!/bin/bash

#Script controls  the TP-Link Mr600 router via snmp
#Script checks if Internet is reachable and
#forces to use LTE interface if wan connection is up without Internet access
#and switches back to wan interface if Internet is reachable

###functions

init ()
{

  #Function initialize the varabiables

  WAN_GATEWAY="192.168.1.1"
  ROUTER_IP="192.168.2.1"
  PING_ADDRESS="8.8.8.8"
  TRIES=2
  HOME_ADDRESS_PREFIX="192.168.2"
  WAN_INTERFACE_ID=22
  SNMP_OID="iso.3.6.1.2.1.2.2.1.7.$WAN_INTERFACE_ID"
  STATUS_UP=1
  STATUS_DOWN=2
  READ_COMMUNITY_STRING="public"
  WRITE_COMMUNITY_STRING="private"
  SNMP_VERSION="2c"
  WAIT_TIME=10
  RECONNECT_TIME=120

}

get_interface_addresses()
{

  #function gathers network adresses

  #counter for result array
  local counter
  counter=0

  #arrays of address
  local result
  result=()

  #ip address
  local ip_address
  ip_address=""

  #get addresses of available interfaces
  for i in $(ls /sys/class/net)
  do

    #increase counter
    ((++counter))

    #extract ip address
    ip_address=$(ip -4 addr show "$i" |grep inet|awk -F 'inet' '{ print $2 }'\
    |cut -d '/' -f1)

    #add result
    result[$counter]=$ip_address

  done

  echo "${result[*]}"

}

test_connection()
{

  #Functions test if network connection is available

  #varabiables
  local status
  status=""

  #test connection
  ping "$1" -c "$2" >/dev/null 2>&1 ; status=$?

  #returning result
  echo "$status"

}

snmp_operation()
{

  #Perform snmp operation

  #result
  local result
  result=""

  #checks oper status of upc wan connection
  if [ "$1" == "status" ]
  then

    result=$(snmpget -v $SNMP_VERSION -c $READ_COMMUNITY_STRING $ROUTER_IP \
    $SNMP_OID|cut -d ":" -f2)

    if [ "$(echo "$result" | wc -c)" -ne 3 ]
    then

      result=""

    fi

  #set interface up
elif [ "$1" == "set_up" ]
  then

    result=$(snmpset -v $SNMP_VERSION -c $WRITE_COMMUNITY_STRING $ROUTER_IP \
    $SNMP_OID i $STATUS_UP|cut -d ":" -f2)

    if [ -n "$(echo "$result" |grep -i error)" ]
    then

      result=""

    fi

  #set interface down
elif [ "$1" == "set_down" ]
  then

    result=$(snmpset -v $SNMP_VERSION -c $WRITE_COMMUNITY_STRING $ROUTER_IP \
    $SNMP_OID i $STATUS_DOWN|cut -d ":" -f2)

    if [ -n "$(echo "$result" |grep -i error)" ]
    then

      result=""

    fi

  fi

  echo "$result"

}

main ()
{

  #Main function with infinite loop

  #initialize
  init "$@"

  #SECONDS
  local SECONDS

  #run infinite loop
  while true
  do

    #seconds
    SECONDS=0

    #check if host is in home network
    if [ -n "$(get_interface_addresses|grep "$HOME_ADDRESS_PREFIX")" ]
    then

      #If ping address and wan gw is unreachable

      if [ "$(test_connection "$PING_ADDRESS" "$TRIES")" -ne 0 ] && \
       [ "$(test_connection "$WAN_GATEWAY" "$TRIES")" -eq 0 ]
      then

        #if interface up then put him down
        if [ "$(snmp_operation "status")" -eq $STATUS_UP ]
        then

          snmp_operation "set_down"

        fi

      fi

    fi

    if [ "$SECONDS" -ge "$RECONNECT_TIME" ]
    then

      #seconds
      SECONDS=0

      #check if host is in home network
      if [ -n "$(get_interface_addresses|grep "$HOME_ADDRESS_PREFIX")" ]
      then

        #If ping address and wan gw is unreachable

        if [ "$(test_connection "$PING_ADDRESS" "$TRIES")" -ne 0 ] && \
        [ "$(test_connection "$WAN_GATEWAY" "$TRIES")" -eq 0 ]
        then

          #if interface up then put him down
          if [ "$(snmp_operation "status" )" -eq $STATUS_DOWN ]
          then

            snmp_operation "set_up"

          fi

        fi

      fi

    fi

    #wait some time
    sleep $WAIT_TIME

  done

}

##### MAIN PART ######

main "$@"
